(function (window) {
	if (!!window.JCTask1) {
		return;
	}

	window.JCTask1 = function (arParams) {
		$('#download-csv').click(function () {
			$.ajax({
				url: arParams.PATH_TO_TEMPLATE + '/ajaxGetUsersCsv.php',
				type: 'POST',
				error: function (data) {
					console.log(data);
				},
				success: function (data) {
					window.location.href = data;
				}
			});
		});
		$('#download-xml').click(function () {
			$.ajax({
				url: arParams.PATH_TO_TEMPLATE + '/ajaxGetUsersXml.php',
				type: 'POST',
				error: function (data) {
					console.log(data);
				},
				success: function (data) {
					var link = document.createElement("a");
					link.download = 'users.xml';
					link.href = data;
					link.click();
				}
			});
		});
	};

})(window);