<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$upload_dir = "/" . COption::GetOptionString("main", "upload_dir", "upload");

$file_path = $upload_dir . "/users.xml";

if (file_exists($_SERVER["DOCUMENT_ROOT"] . $file_path)) {
	unlink($_SERVER["DOCUMENT_ROOT"] . $file_path);
}

$xml = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?><users/>");

//получаем всех пользователей
$arUsers = [];

$by = "ID";
$order = "asc";

$arParameters = [
	false,
	false,
	'FIELDS' => [
		'LOGIN',
		'EMAIL',
		'NAME',
		'SECOND_NAME',
		'LAST_NAME'
	]
];

$rsUsers = CUser::GetList($by, $order, [], $arParameters);

while ($arUser = $rsUsers->Fetch()) {
	$arUsers[] = [
		'login' => $arUser['LOGIN'],
		'email' => $arUser['EMAIL'],
		'name' => $arUser['NAME'],
		'second_name' => $arUser['SECOND_NAME'],
		'last_name' => $arUser['LAST_NAME'],
	];
}

//рекурсивная функция преобразует массив в xml
function array_to_xml($array, &$xml)
{
	foreach ($array as $key => $value) {
		if (is_array($value)) {
			if (!is_numeric($key)) {
				$subnode = $xml->addChild("$key");
				array_to_xml($value, $subnode);
			} else {
				$subnode = $xml->addChild("user");
				array_to_xml($value, $subnode);
			}
		} else {
			if (!is_numeric($key)) {
				$xml->addChild("$key", "$value");
			} else {
				$xml->addChild("user", "$value");
			}
		}
	}
}

array_to_xml($arUsers, $xml);
$xml->saveXML($_SERVER["DOCUMENT_ROOT"] . $file_path);

echo $file_path;