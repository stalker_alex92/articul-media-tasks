<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

foreach ($arResult["USERS"] as $arUser) {
	?>
    <p>
		<?= GetMessage("TASK2_FIO") . ': ' . $arUser['FULL_NAME'] ?><br>
		<?= GetMessage("TASK2_USER_LOGIN") . ': ' . $arUser['LOGIN'] ?><br>
		<?= GetMessage("TASK2_USER_EMAIL") . ': ' . $arUser['EMAIL'] ?>
    </p>
	<?
}

?><?= $arResult["NAV_STRING"] ?>
<br><br>
<a href="javascript:void(0);" id="download-csv"><?= GetMessage("TASK2_DOWNLOAD_CSV") ?></a><br><br>
<a href="javascript:void(0);" id="download-xml"><?= GetMessage("TASK2_DOWNLOAD_XML") ?></a>

<?
$arJSParams = array(
	'PATH_TO_COMPONENT' => $componentPath,
	'PATH_TO_TEMPLATE' => $templateFolder
);
?>
<script type="text/javascript">
	BX.ready(function () {
		new JCTask1(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
	});
</script>
