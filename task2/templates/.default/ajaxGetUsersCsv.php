<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/classes/general/csv_data.php");

$upload_dir = "/" . COption::GetOptionString("main", "upload_dir", "upload");

$file_path = $upload_dir . "/users.csv";

if (file_exists($_SERVER["DOCUMENT_ROOT"] . $file_path)) {
	unlink($_SERVER["DOCUMENT_ROOT"] . $file_path);
}

$csvFile = new CCSVData('R');
$delimiter = ";";
$csvFile->SetDelimiter($delimiter);

$arrHeaderCSV = [
	'LOGIN',
	'EMAIL',
	'NAME',
	'SECOND_NAME',
	'LAST_NAME'
];

$csvFile->SaveFile($_SERVER["DOCUMENT_ROOT"] . $file_path, $arrHeaderCSV);

$by = "ID";
$order = "asc";

$arParameters = [
	false,
	false,
	'FIELDS' => [
		'LOGIN',
		'EMAIL',
		'NAME',
		'SECOND_NAME',
		'LAST_NAME'
	]
];

$rsUsers = CUser::GetList($by, $order, [], $arParameters);

$csvString = [];
while ($arUser = $rsUsers->Fetch()) {
	$csvString = [
		mb_convert_encoding($arUser['LOGIN'], "Windows-1251"),
		mb_convert_encoding($arUser['EMAIL'], "Windows-1251"),
		mb_convert_encoding($arUser['NAME'], "Windows-1251"),
		mb_convert_encoding($arUser['SECOND_NAME'], "Windows-1251"),
		mb_convert_encoding($arUser['LAST_NAME'], "Windows-1251")
	];
	$csvFile->SaveFile($_SERVER["DOCUMENT_ROOT"] . $file_path, $csvString);
}

$csvFile->CloseFile();

echo $file_path;