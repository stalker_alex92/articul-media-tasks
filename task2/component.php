<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */

/** @global CMain $APPLICATION */


if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$CACHE_ID = $_REQUEST['PAGEN_1'];

if ($this->StartResultCache($arParams['CACHE_TIME'], $CACHE_ID)) {
	$arResult['USERS'] = [];

	$by = "ID";
	$order = "asc";
	$arSelect = [
		'LOGIN',
		'EMAIL',
		'NAME',
		'SECOND_NAME',
		'LAST_NAME'
	];
	$arParameters = [
		false,
		false,
		'FIELDS' => [
			'LOGIN',
			'EMAIL',
			'NAME',
			'SECOND_NAME',
			'LAST_NAME'
		]
	];

	$rsUsers = CUser::GetList($by, $order, [], $arParameters);

	$rsUsers->NavStart(2);
	$arResult['NAV_STRING'] = $rsUsers->GetNavPrint(GetMessage("TASK2_NAV_STRING_TITLE"));
	while ($arUser = $rsUsers->Fetch()) {
		$arResult['USERS'][] = [
			'LOGIN' => $arUser['LOGIN'],
			'EMAIL' => $arUser['EMAIL'],
			'FULL_NAME' => trim(trim($arUser['NAME'] . ' ' . $arUser['SECOND_NAME']) . ' ' . $arUser['LAST_NAME'])
		];
	}

	$this->IncludeComponentTemplate();
}