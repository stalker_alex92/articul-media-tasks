<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = [
	"NAME" => GetMessage("TASK1_NAME"),
	"DESCRIPTION" => GetMessage("TASK1_DESCRIPTION"),
	"SORT" => 30,
	"CACHE_PATH" => "Y",
	"PATH" => [
		"ID" => "news",
	],
];

?>