<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */

/** @global CMain $APPLICATION */

use Bitrix\Main\Loader as Loader;

$name = $_GET['name'];

if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$CACHE_ID = SITE_ID . '|' . $name;


if ($this->StartResultCache($arParams['CACHE_TIME'], $CACHE_ID)) {

	if (!Loader::includeModule('iblock')) {
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	//выбираем элементы
	$arResult['ITEMS'] = [];

	$arSelect = [
		'NAME',
		'IBLOCK_SECTION_ID'
	];

	$arFilter = [
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
		'ACTIVE' => 'Y',
		'NAME' => '%' . $name . '%'
	];

	$res = CIBlockElement::GetList(['NAME' => 'ASC'], $arFilter, false, ['nTopCount' => 10], $arSelect);
	while ($ob = $res->GetNextElement()) {
		$arFields = $ob->GetFields();
		$arResult['ITEMS'][] = [
			'NAME' => $arFields['NAME'],
			'IBLOCK_SECTION_ID' => $arFields['IBLOCK_SECTION_ID']
		];
	}

	if (count($arResult['ITEMS']) > 0) {
		$sections = [];

		//выбираем разделы
		$arSelect = [
			'ID',
			'NAME'
		];
		$arFilter = [
			'IBLOCK_ID' => $arParams['IBLOCK_ID'],
			'ACTIVE' => 'Y'
		];

		$rsSection = CIBlockSection::GetList(['ID' => 'ASC'], $arFilter, false, $arSelect);
		while ($arSection = $rsSection->GetNext()) {
			$sections[$arSection['ID']] = $arSection['NAME'];
		}

		if (count($sections) > 0) {
			foreach ($arResult['ITEMS'] as &$arItem) {
				if ($sections[$arItem['IBLOCK_SECTION_ID']] != null) {
					$arItem['SECTION_NAME'] = $sections[$arItem['IBLOCK_SECTION_ID']];
				}
			}
		}

		unset($sections);
	}

	$this->IncludeComponentTemplate();
}