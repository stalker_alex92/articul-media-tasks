<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (count($arResult["ITEMS"]) > 0) {
	foreach ($arResult["ITEMS"] as $arItem) {
		?>
        <p>
			<?=GetMessage("TASK1_ELEMENT_NAME")?>: <?= $arItem['NAME']; ?><br>
			<?if(isset($arItem['SECTION_NAME'])){?>
                <?=GetMessage("TASK1_SECTION_NAME")?>: <?= $arItem['SECTION_NAME'] ?>
            <?}?>
        </p>
		<?
	}
} else {
	?><p><?=GetMessage("TASK1_EMPTY_ELEMENTS")?></p><?
}