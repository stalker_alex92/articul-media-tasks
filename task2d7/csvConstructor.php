<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\UserTable;

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=users.csv");
header("Pragma: no-cache");
header("Expires: 0");

$delimiter = ";";

//header
echo "Login" . $delimiter . "Email" . $delimiter . "Name" . $delimiter . "Second name" . $delimiter . "Last name" . "\n";

$arUsers = [];
$dbUsers = UserTable::getList(
	[
		"count_total" => true,
		"offset" => 0,
		"limit" => 0,
	]
);

while ($arUser = $dbUsers->fetch()) {
	$arUsers[] = [
		"LOGIN" => $arUser["LOGIN"],
		"EMAIL" => $arUser["EMAIL"],
		"NAME" => $arUser["NAME"],
		"SECOND_NAME" => $arUser["SECOND_NAME"],
		"LAST_NAME" => $arUser["LAST_NAME"],
	];
}

foreach ($arUsers as $arUser) {
	echo mb_convert_encoding($arUser["LOGIN"], "Windows-1251") . $delimiter .
		mb_convert_encoding($arUser["EMAIL"], "Windows-1251") . $delimiter .
		mb_convert_encoding($arUser["NAME"], "Windows-1251") . $delimiter .
		mb_convert_encoding($arUser["SECOND_NAME"], "Windows-1251") . $delimiter .
		mb_convert_encoding($arUser["LAST_NAME"], "Windows-1251") . "\n";
}