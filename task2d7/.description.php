<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = [
	"NAME" => GetMessage("TASK2_NAME"),
	"DESCRIPTION" => GetMessage("TASK2_DESCRIPTION"),
	"SORT" => 30,
	"CACHE_PATH" => "Y",
	"PATH" => [
		"ID" => "news",
	],
];

?>