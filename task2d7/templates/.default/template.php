<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

foreach ($arResult["USERS"] as $arUser) {
	?>
    <p>
		<?= GetMessage("TASK2_FIO") . ': ' . $arUser['FULL_NAME'] ?><br>
		<?= GetMessage("TASK2_USER_LOGIN") . ': ' . $arUser['LOGIN'] ?><br>
		<?= GetMessage("TASK2_USER_EMAIL") . ': ' . $arUser['EMAIL'] ?>
    </p>
	<?
}

?>
<?
$APPLICATION->IncludeComponent(
	"bitrix:main.pagenavigation",
	"",
	[
		"NAV_OBJECT" => $arResult['NAVIGATION']
	],
	false
);
?>
<br><br>
<a href="<?= $componentPath . '/csvConstructor.php' ?>" download="users.csv"><?= GetMessage("TASK2_DOWNLOAD_CSV") ?></a>
<br><br>
<a href="<?= $componentPath . '/xmlConstructor.php' ?>" download="users.xml"><?= GetMessage("TASK2_DOWNLOAD_XML") ?></a>