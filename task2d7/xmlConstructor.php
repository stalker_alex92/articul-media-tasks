<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\UserTable;

header('Content-Type: application/xml');
?>
    <users>
		<?
		$arUsers = [];
		$dbUsers = UserTable::getList(
			[
				"count_total" => true,
				"offset" => 0,
				"limit" => 0,
			]
		);

		while ($arUser = $dbUsers->fetch()) {
			$arUsers[] = [
				'LOGIN' => $arUser['LOGIN'],
				'EMAIL' => $arUser['EMAIL'],
				'NAME' => $arUser['NAME'],
				'SECOND_NAME' => $arUser['SECOND_NAME'],
				'LAST_NAME' => $arUser['LAST_NAME'],
			];
		}

		foreach ($arUsers as $arUser) {
			?>
            <user>
                <login><?= $arUser['LOGIN'] ?></login>
                <email><?= $arUser['EMAIL'] ?></email>
                <name><?= $arUser['NAME'] ?></name>
                <second_name><?= $arUser['SECOND_NAME'] ?></second_name>
                <last_name><?= $arUser['LAST_NAME'] ?></last_name>
            </user>
			<?
		}
		?>
    </users>
<?