<?php

use \Bitrix\Main\UI\PageNavigation,
	\Bitrix\Main\UserTable;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

class CUserList extends CBitrixComponent
{
	public function onPrepareComponentParams($params)
	{
		$result = ["CACHE_TIME" => isset($params["CACHE_TIME"]) ? $params["CACHE_TIME"] : 36000000];
		return $result;
	}

	public function executeComponent()
	{

		$this->arResult['NAVIGATION'] = new PageNavigation("nav-more-users");
		$this->arResult['NAVIGATION']->allowAllRecords(true)->setPageSize(2)->initFromUri();

		$dbUsers = UserTable::getList(
			[
				"count_total" => true,
				"offset" => $this->arResult['NAVIGATION']->getOffset(),
				"limit" => $this->arResult['NAVIGATION']->getLimit(),
				"cache" => ["ttl" => $this->arParams['CACHE_TIME']]
			]
		);

		$this->arResult['NAVIGATION']->setRecordCount($dbUsers->getCount());

		while ($arUser = $dbUsers->fetch()) {
			$this->arResult['USERS'][] = [
				'LOGIN' => $arUser['LOGIN'],
				'EMAIL' => $arUser['EMAIL'],
				'FULL_NAME' => trim(trim($arUser['NAME'] . ' ' . $arUser['SECOND_NAME']) . ' ' . $arUser['LAST_NAME'])
			];
		}

		$this->IncludeComponentTemplate();
	}
}