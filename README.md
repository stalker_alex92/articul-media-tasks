# Компоненты для решения задания articul media

##Задание: 

1) Сделать компонент для Битрикса, который берет из ссылки GET-параметр "name" и выводит на странице 10 элементов инфоблока, в названии которых содержится значение "name" из адресной строки. Выборка должна кешироваться.
Вывести: название элемента и название раздела.

2) Реализовать компонент с применением технологий ядра D7, который выводит список зарегистрированных пользователей с постраничной навигацией, выборки пользователей должны кешироваться. Постраничная навигация должна осуществляться Ajax-ом. Сделать возможность выгрузки пользователей в csv и xml формат.

Исходники можно прислать в zip-архиве или (желательно) выложить в публичный репозиторий bitbucket или github.

##Решение:

Первую задачу решает компонент task1, вторую task2, taskd7 - используя D7.

####Код вызова первого компонента:

<?
$APPLICATION->IncludeComponent(
	"siplay:task1",
	".default",
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "template",
		"IBLOCK_ID" => "1",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000"
	),
	false
);
?>

####Код вызова второго компонента:

<?
$APPLICATION->IncludeComponent(
	"siplay:task2",
	".default",
	array(
		'AJAX_MODE' => 'Y',
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
	),
	false
);
?>

####Код вызова второго компонента D7:

<?
$APPLICATION->IncludeComponent(
	"siplay:task2d7",
	".default",
	array(
		'AJAX_MODE' => 'Y',
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
	),
	false
);
?>

Компоненты расположены на сервере: http://template.dutov.isintex-developers.ru/
